# GSI

Solución en JAVA del Laboratorio de la clase Algoritmos y Complejidad de la Universidad del Norte Semesrte 201810.

## Problema
Diseñar un programa en JAVA (NetBeans IDE 8.2) representativo de un Generador de Sistemas de Información GSI, el cual en su capa Back_end está compuesto por: 

1. Generar random un conjunto de  n  registros. Cada registro está compuesto por Ci campos (1<=i<=m), el primero de los cuales es una llave única; cada campo es del tipo numérico o alfabético de longitud  k. El generador de registros se debe realizar para grandes valores de datos de entrada. 
2.	Actuar sobre los datos generados las funciones de:

	a.	Direccionar el conjunto de datos generados en la estructura de datos seleccionada. 
	
	b.	Ordenar el conjunto de datos por el campo clave.
	
	c.	Buscar en el conjunto de registros por el campo clave o por cualquier otro campo del registro. 
	
3.	Procesar el conjunto de registros para generar las informaciones: 

	a.	El valor máximo o el valor mínimo del conjunto de registros correspondiente al campo Ci. 
	
	b.	El promedio de un conjunto registros de un campo numérico dado Ci. 
	
	c.	La moda del conjunto de datos de un campo Ci

## Progreso

### Generador

- [X] Generador de n registros
- [X] Cantidad m de campos cada uno de longitud k diferentes
- [X] Generador de llaves numéricas únicas en el primer campo

### Actuador

- [X] Estructura de datos que almacenará los datos generados
- [ ] Ordenar los datos por el campo clave (1)
- [ ] Buscador de registros por cualquier campo

### Procesador
- [ ] Máximo o mínimo de un conjunto del campo i
- [ ] Promedio de un conjunto de un campo i
- [ ] Moda de conjunto de un campo i
