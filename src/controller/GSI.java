package controller;

import java.awt.Component;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Vector;
import model.ModeloCampo;
import model.Registro;
import view.CampoPanel;
import view.Formato;
import view.View;

/**
 *
 * @author dmfigueroa
 */
public class GSI {

    public static final ArrayList<ModeloCampo> campos = new ArrayList<>();
    public static final HashSet<Registro> registros = new HashSet<>();
    public static final java.util.Random RN = new java.util.Random();
    public static final HashSet<BigInteger> ids = new HashSet<>();
    public static Formato formato;

    public static void main(String args[]) {
        formato = new Formato();
        formato.setLocationRelativeTo(null);
        formato.setVisible(true);
    }

    public static void generateRegistros(int numeroRegistros) {
        long time = System.nanoTime();
        for (int i = 0; i < numeroRegistros; i++) {
            registros.add(new Registro());
        }
        long end = System.nanoTime();
        System.out.println("Registros generados en " + (end - time) + " nanosegundos");
    }

    public static void saveCampos() {
        Component[] components = Formato.panelCampos.getComponents();
        for (Component component : components) {
            CampoPanel campoPanel = (CampoPanel) component;
            int length = campoPanel.getLength();
            String info = campoPanel.getInfo();
            boolean alfanumerico = campoPanel.isAlfanumerico();
            campos.add(new ModeloCampo(length, info, alfanumerico));
        }
    }

    public static void openView() {
        View view = new View();
        view.setVisible(true);
    }

    public static Vector getCamposNames() {
        Vector result = new Vector();
        for (int i = 0; i < campos.size(); i++) {
            result.add(campos.get(i).getName());
        }
        return result;
    }

}
