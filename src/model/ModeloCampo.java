/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author David
 */
public class ModeloCampo {

    private final int length;
    private final String name;
    private final boolean alfabetic;

    public ModeloCampo(int length, String name, boolean alfanumeric) {
        this.alfabetic = alfanumeric;
        this.name = name;
        this.length = length;
    }

    @Override
    public String toString(){
        return name+": "+""+length+"|"+((alfabetic)?"Alfanumerico":"Numerico");
    }

    public int getLength() {
        return length;
    }

    public String getName() {
        return name;
    }

    public boolean isAlfabetic() {
        return alfabetic;
    }
}
