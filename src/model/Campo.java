/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.math.BigInteger;

/**
 *
 * @author David
 */
public class Campo {
    private final int length;
    private final BigInteger id;
    private final String info;
    private final boolean alfanumeric;
    
    public Campo(int length, String info, boolean alfanumeric){
        this.length = length;
        this.info = info;
        this.alfanumeric = alfanumeric;
        this.id = null;
    }
    
    public Campo(int length, BigInteger id, boolean alfanumeric){
        this.length = length;
        this.info = null;
        this.alfanumeric = false;
        this.id = id;
    }
    public String getInfo(){
        if (this.id == null) {
            return this.info;
        }else{
            return this.id.toString();
        }
    }
    
    public String toString(){
        return this.getInfo();
    }
}
