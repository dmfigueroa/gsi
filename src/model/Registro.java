/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.GSI;
import java.math.BigInteger;
import java.util.ArrayList;

/**
 *
 * @author David
 */
public class Registro {

    private final ArrayList<Campo> campos = new ArrayList<>();
    private final char[] alfabethicString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
    private final char[] numberString = "0123456789".toCharArray();

    public Registro() {
        for (int i = 0; i < GSI.campos.size(); i++) {
            ModeloCampo model = GSI.campos.get(i);
            if (i == 0) {
                BigInteger id = newId(model.getLength());
                campos.add(new Campo(model.getLength(), id, false));
            } else {
                if (model.isAlfabetic()) {
                    campos.add(new Campo(model.getLength(), generateString(GSI.RN.nextInt(model.getLength() - 1) + 2), true));
                } else {
                    campos.add(new Campo(model.getLength(), generateInt(GSI.RN.nextInt(model.getLength() - 1) + 2), false));
                }
            }
        }
    }

    private BigInteger newId(int length) {
        BigInteger randomID;
        do {
            randomID = generateRandomID(length);
        } while (GSI.ids.contains(randomID));
        GSI.ids.add(randomID);
        return randomID;
    }

    private BigInteger generateRandomID(int length) {
        char[] result = new char[length];
        for (int i = 0; i < length; i++) {
            result[i] = numberString[GSI.RN.nextInt(numberString.length)];
        }
        return new BigInteger(new String(result)).abs();
    }

    private String generateString(int length) {
        char[] result = new char[length];
        for (int i = 0; i < length; i++) {
            result[i] = alfabethicString[GSI.RN.nextInt(alfabethicString.length)];
        }
        return new String(result);
    }

    private String generateInt(int length) {
        return generateRandomID(length) + "";
    }

    public ArrayList<Campo> getCampos() {
        return campos;
    }

    public int getCamposLength() {
        return campos.size();
    }

    @Override
    public String toString() {
        String result = "";
        for (int i = 0; i < campos.size(); i++) {
            result += campos.get(i).toString() + "|";
        }
        return result;
    }

    public BigInteger getID() {
        return new BigInteger(this.campos.get(0).getInfo());
    }
}
